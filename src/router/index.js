import Router from 'vue-router'

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Homepage',
      component: require('../components/Homepage.vue')
    },
    {
      path: '/connexion',
      name: 'Connexion',
      component: require('../components/Authentification.vue')
    },
    {
      path: '/clients',
      name: 'Clients',
      component: require('../components/Clients/Clients.vue')
    },
    {
      path: '/code-affaires',
      name: 'CodeAffaires',
      component: require('../components/CodeAffaires/CodeAffaires.vue')
    },
    {
      path: '/plannings',
      name: 'Plannings',
      component: require('../components/Plannings/Plannings.vue')
    },
    {
      path: '/planning',
      name: 'PlanningShow',
      component: require('../components/Plannings/PlanningShow.vue')
    },
    {
      path: '/utilisateur',
      name: 'Utilisateurs',
      component: require('../components/Users/Users.vue')
    },
    {
      path: '/roles',
      name: 'Roles',
      component: require('../components/Roles/Roles.vue')
    },
    {
      path: '/historique',
      name: 'Historique',
      component: require('../components/History/History.vue')
    },
    {
      path: '*',
      redirect: '/'
    }
  ]
})

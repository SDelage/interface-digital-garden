// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import router from './router'
import App from './App'
import Router from 'vue-router'
import VueResource from 'vue-resource'
import Datepicker from 'vuejs-datepicker'
import Navbar from './components/Navbar'

Vue.use(VueResource)
Vue.use(Router)
Vue.http.options.root = 'http://localhost:8080//API-DigitalGarden/web/app_dev.php'

Vue.http.interceptors.push((request, next) => {
  if (sessionStorage.AuthToken !== undefined) {
    request.headers.set('X-Auth-Token', sessionStorage.AuthToken)
  }
  next((response) => {
    if (response.status === 401) {
      sessionStorage.clear()
      window.location.replace('/connexion')
    }
    if (request.after) {
      request.after.call(this, response)
    }
  })
})

Vue.component('datepicker', Datepicker)
Vue.component('Navbar', Navbar)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})

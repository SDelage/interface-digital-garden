export default class RestResource {
  // START Add All Connection
  $rauth = this.$resource('auth-tokens{/id}', {}, {}, {
    before: () => { this.loading = true },
    after: () => { this.loading = false }
  })
  $rusers = this.$resource('users{/id}', {}, {
    saveuser: { method: 'PATCH', url: 'users{/id}' }
  }, {
    before: () => { this.loading = true },
    after: () => { this.loading = false }
  })
  $rroles = this.$resource('roles{/id}', {}, {
    saverole: { method: 'PATCH', url: 'roles{/id}' }
  }, {
    before: () => { this.loading = true },
    after: () => { this.loading = false }
  })
  $rplannings = this.$resource('plannings{/id}', {}, {}, {
    before: () => { this.loading = true },
    after: () => { this.loading = false }
  })
  $rtaches = this.$resource('taches{/id}', {}, {
    savetache: {method: 'PATCH', url: 'taches{/id}'}
  }, {
    before: () => { this.loading = true },
    after: () => { this.loading = false }
  })
  $rcodeaffaires = this.$resource('code-affaires{/id}', {}, {
    savecodeaffaire: {method: 'PATCH', url: 'code-affaires{/id}'}
  }, {
    before: () => { this.loading = true },
    after: () => { this.loading = false }
  })
  $rclients = this.$resource('clients{/id}', {}, {
    saveclient: { method: 'PATCH', url: 'clients{/id}' }
  }, {
    before: () => { this.loading = true },
    after: () => { this.loading = false }
  })
  // END Add All Connection
}
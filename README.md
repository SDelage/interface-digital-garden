# interfacedigitalgarden

> Client pour l'API Digitalgarden

## Build Setup

+ Attention bien penser a modifier l'url d'appel de l'API dans le main.js

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Roadmap

###### Ajout gestion utilisateur
+ bouton de renvoie d'email
+ modification du profil (image profil -> connection site avatar ?)
+ stat user sur la homepage

###### Ajout vue statistique temps passer
+ voir le temps passé sur semaine / mois / plage de semaine définit par le user
+ filtre par client (AND/OR) code affaire

###### Amélioration UX
+ Formulaire
+ Police

###### Planning Show
+ Style commentaire + temps réel passé

###### Bug Trace
+ Filtre et Push sur User

